package task2;

import task1.GeomFigure;

// класс многоугольники
public class Polygon extends GeomFigure implements WithAngles {
    protected int numApexes;
    protected Pair[] apexes; //подразумевается, что вершины перечислены в порядке обхода

    public Polygon(Pair[] apexes_) {
        this.numApexes = apexes_.length;
        this.apexes = apexes_;
    }

    public Pair getApexByIndex(int index) {
        if (index >= numApexes) return null;
        return apexes[index];
    }

    public void showAllApexes() {
        for (Pair apex : apexes) {
            System.out.println(apex.getX() + "  " + apex.getY());
        }
    }

    public double GetArea() {
        double area = 0;
        for (int i = 0; i < numApexes; i++) {
            area += apexes[i].getX() * (apexes[(i + 1 + numApexes) % numApexes].getY() - apexes[(i - 1 + numApexes) % numApexes].getY());
        }
        area = 0.5 * Math.abs(area);
        return area;
    }

    public double GetPerimeter() {
        double perimeter = 0;
        for (int i = 0; i < numApexes - 1; i++) {
            perimeter += apexes[i].lengthToPoint(apexes[i + 1]);
        }
        perimeter += apexes[0].lengthToPoint(apexes[numApexes - 1]);
        return perimeter;
    }

}
