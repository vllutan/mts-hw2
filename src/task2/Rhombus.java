package task2;

public class Rhombus extends Polygon implements EqualSides {
    private double sideLength;

    public Rhombus(Pair[] apexes_) {
        super(apexes_);
        sideLength = apexes[0].lengthToPoint(apexes[3]);
        checkEqualSides();
    }

    public boolean checkEqualSides() {
        for (int i = 0; i < 3; i++) {
            if (sideLength != apexes[i].lengthToPoint(apexes[i + 1])) {
                return false;
            }
        }
        return true;
    }

    public double getSideLength() { return this.sideLength; }

    // выводит результаты всех возможных методов класса
    public void printAllInfo() {
        System.out.println("Rhombus:\nHas equal side? " + this.checkEqualSides() + ", with side = " + this.sideLength +
                "\nArea: " + this.GetArea() + "\nPerimeter: " + this.GetPerimeter() + "\n  All apexes: total - " + this.numApexes);
        this.showAllApexes();
    }
}
