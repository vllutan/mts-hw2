package task2;

// класс для пары значений координат x и y
public class Pair {
    private double x;
    private double y;

    public Pair(double first, double second) {
        this.x = first;
        this.y = second;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    // расстояние до другой точки
    public double lengthToPoint(Pair other) {
        return Math.sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
    }
}
