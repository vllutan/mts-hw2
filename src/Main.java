import task1.Circle;
import task1.Ellipse;
import task2.Pair;
import task2.Polygon;
import task2.Rhombus;

public class Main {

    public static boolean areDoublesEqual(double x, double y, double eps){
        return Math.abs(x - y) <= eps;
    }

    public static void main(String[] args) {
        double eps = 0.000001;

        Circle c1 = new Circle(1, 1, 5, "green");
        if(! areDoublesEqual(c1.GetPerimeter(), 31.415927, eps)) System.out.println("Circle1 perimeter wrong");
        if(! areDoublesEqual(c1.GetArea(), 78.539816, eps)) System.out.println("Circle1 area wrong");

        Circle c2 = new Circle(0, 0, 0, "green");
        if(! areDoublesEqual(c2.GetPerimeter(), 0, eps)) System.out.println("Circle2 perimeter wrong");
        if(! areDoublesEqual(c2.GetArea(), 0, eps)) System.out.println("Circle2 area wrong");

        Ellipse e1 = new Ellipse(2, 2, 4, 2, 10, 5, "yellow");
        if(! areDoublesEqual(e1.GetPerimeter(), 10.805309, eps)) System.out.println("Ellipse1 perimeter wrong");
        if(! areDoublesEqual(e1.GetArea(), 157.079632, eps)) System.out.println("Ellipse1 area wrong");

        Ellipse e2 = new Ellipse(e1);
        if(! areDoublesEqual(e2.GetPerimeter(),  0, eps)) System.out.println("Ellipse2 perimeter wrong"); //10.805309;
        if(! areDoublesEqual(e2.GetArea(), 157.079632, eps)) System.out.println("Ellipse2 area wrong");

        e2.setColor("red");
        System.out.println("e1 color " + e1.getColor() + " , e2 color " + e2.getColor());

        if(!e2.getColor().equals("red")) System.out.println("Ellipse2 color wrong");
        if(!e1.getColor().equals("yellow")) System.out.println("Ellipse1 color wrong");

        System.out.println("e2 perimeter:  " + e2.GetPerimeter());

        // ---------------------------

        Pair[] apexes1 = new Pair[]{new Pair(1,1), new Pair(2,2), new Pair(0,2)}; // треугольник
        Polygon p1 = new Polygon(apexes1);
        p1.showAllApexes();
        System.out.println("p1 area and perimeter: " + p1.GetArea() + " , " + p1.GetPerimeter());

        Pair[] apexes2 = new Pair[]{new Pair(0,0), new Pair(0,2), new Pair(2,2), new Pair(2,0)}; // квадрат
        Polygon p2 = new Polygon(apexes2);
        p2.showAllApexes();
        System.out.println("p2 area and perimeter: " + p2.GetArea() + " , " + p2.GetPerimeter());

        Pair[] apexes3 = new Pair[]{new Pair(0,0), new Pair(0,2), new Pair(1,-1), new Pair(2,2), new Pair(2,0)};
        Polygon p3 = new Polygon(apexes3); //плохой многоугольник
        p3.showAllApexes();
        System.out.println("p3 area and perimeter: " + p3.GetArea() + " , " + p3.GetPerimeter()); // выводит неправильные данные

        Pair[] apexes4 = new Pair[]{new Pair(0,0), new Pair(0,2), new Pair(0.67,0), new Pair(1.33,0), new Pair(2,2),
                                    new Pair(2,0), new Pair(1.34,0), new Pair(1,-1), new Pair(0.66,0)};
        Polygon p4 = new Polygon(apexes4); // р3, заданный нормально
        p4.showAllApexes();
        System.out.println("p4 area and perimeter: " + p4.GetArea() + " , " + p4.GetPerimeter()); // значения верные

        Pair[] apexes5 = new Pair[]{new Pair(0,0), new Pair(-1,2), new Pair(0,4), new Pair(1,2)};
        Rhombus r1 = new Rhombus(apexes5);
        r1.printAllInfo();

        //System.out.println("All done");
    }
}