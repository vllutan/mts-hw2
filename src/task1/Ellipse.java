package task1;

// класс эллипс
public class Ellipse extends GeomFigure {
    private double centerX1;
    private double centerY1;
    private double centerX2;
    private double centerY2;
    private double radiusBig;
    private double radiusSmall;
    private String color;

    public Ellipse(double centerX1, double centerY1,
            double centerX2, double centerY2,
            double radBig, double radSmall, String color) {
        this.centerX1 = centerX1;
        this.centerY1 = centerY1;
        this.centerX2 = centerX2;
        this.centerY2 = centerY2;
        this.radiusBig = radBig;
        this.radiusSmall = radSmall;
        this.color = color;
    }

    public Ellipse(Ellipse otherEllipse) {
        this.centerX1 = otherEllipse.centerX1;
        this.centerX2 = otherEllipse.centerX2;
        this.centerY1 = otherEllipse.centerY1;
        this.centerY2 = otherEllipse.centerY2;
        this.radiusBig = otherEllipse.radiusBig;
        this.radiusSmall = otherEllipse.radiusSmall;
        this.color = otherEllipse.color;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }

    public double GetPerimeter() {
        return (Math.PI * radiusBig * radiusSmall + radiusBig - radiusSmall) / (radiusBig + radiusSmall);
    }

    public double GetArea() {
        return Math.PI * radiusBig * radiusSmall;
    }

}
