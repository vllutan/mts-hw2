package task1;

// класс круг
public class Circle extends GeomFigure {
    private double centerX;
    private double centerY;
    private double radius;
    private String color;

    public Circle(double centerX, double centerY, double radius, String color) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }

    public double getX(){ return this.centerX; }

    public double getY() { return this.centerY; }

    public double getRadius(){ return this.radius; }

    public double GetPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    public double GetArea() {
        return Math.PI * this.radius * this.radius;
    }

}
