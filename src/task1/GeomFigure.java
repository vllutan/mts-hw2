package task1;

// задание 1.1
// базовый класс фигура
public abstract class GeomFigure {
    public abstract double GetPerimeter();

    public abstract double GetArea();
}
